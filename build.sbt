ThisBuild / version      := "0.0.1"
ThisBuild / scalaVersion := "2.12.6"
ThisBuild / organization := "com.hysoaka"

val akkaActor = "com.typesafe.akka" %% "akka-actor" % "2.5.13"
val akkaStream = "com.typesafe.akka" %% "akka-stream" % "2.5.13"

lazy val simpleAkkaStream = (project in file("."))
    .settings(
        name := "Simple Akka Stream",
        libraryDependencies ++= Seq(akkaStream, akkaActor),
    )
