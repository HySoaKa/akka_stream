package com.hysoaka

import java.nio.file.Paths
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl._

object TransformingStreamsApplication extends App {

    implicit val actorSystem = ActorSystem("TransformingStream")
    implicit val actorMaterializer = ActorMaterializer()

    val maxGroups = 100
    val path = Paths.get("src/main/resources/gzipped-file.gz")

// FileIO is a Source stage provided in Akka Streams library

    val stream = FileIO.fromPath(path) // Emiting through the stream in ByteString
        .via(Compression.gunzip()) // Uncompressing the file (Flow)
        .map(_.utf8String.toUpperCase) // Mapping ByteStrings to UTF8
        .mapConcat(_.split(" ").toList) // Spliting by spaces
        .collect { case w if w.nonEmpty => // Collecting each non-empty element
            w.replaceAll(System.lineSeparator(), "")
            .replaceAll("""[\p{Punct}&&[^.]]""", "") } // Replacing all new lines and punctuation signs
        .groupBy(maxGroups, identity) // grouping our elements, separating output streams for each element key
        .map(_ -> 1) // Mapping each word to a tuple (word, 1)
        .reduce((l, r) => (l._1, l._2 + r._2)) // Using reduce to sum all elements in each substream
        .mergeSubstreams // Flatening the substream 
        .to(Sink.foreach(println))
    
    stream.run()

}