package com.hysoaka

import akka.stream.{Attributes, Inlet, SinkShape}
import akka.stream.stage._

import scala.concurrent.duration._

// Custom sink stage
class WordCounterSink extends GraphStage[SinkShape[String]] {
    val in: Inlet[String] = Inlet("WordCounterSink")
    override val shape = SinkShape(in)

    // Sink receives elements from a stream
    // -> Implement TimerGraphStageLogic with InHandler
    // TimerGraphStageLogic provides APIs to schedule msgs within the stage
    // To request for more elmts from the upstream we use pull and grab methods
    // pull and grab help to manage back pressure
    override def createLogic(inheritedAttributes: Attributes) = new TimerGraphStageLogic(shape) {
        var counts = Map.empty[String, Int].withDefaultValue(0)

        override def preStart() = {
            // Schedule a None timer key to be periodically sent to the stage every 5 seconds
            schedulePeriodically(None, 5 seconds)
            // pull requests an element from the stream
            // calling it twice before an element arrived make it throw an exception
            pull(in)  
        }

        setHandler(in, new InHandler {
            override def onPush() = {
                // grab returns the element that comes from the stream
                val word = grab(in)
                counts += word -> (counts(word) + 1)
                pull(in)
            }
        })

        // Every time the scheduler is triggered onTimer is called with the timer key
        override def onTimer(timerKey: Any) =
            println(s"At ${System.currentTimeMillis()} count map is $counts")
    }

}