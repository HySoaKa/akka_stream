package com.hysoaka

// Source: the entry point to the stream
// Sink: the exit point of the stream
// Flow: the component responsible for manipulating the elements of the stream

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}

object SimpleStreamsApplication extends App {
    implicit val actorSystem = ActorSystem("SimpleStream") // Needed by actorMaterializer
    implicit val actorMaterializer = ActorMaterializer() // Responsible for creating the underlying actors with the specific functionality you define in the stream

    val fileList = List("src/main/resources/testfile1.txt",
        "src/main/resources/testfile2.txt",
        "src/main/resources/testfile3.txt")
    

// Stream = Graph where nodes = (Sources, Flows, Sinks) called stages + connections = link each stage with the other

    // stream has a type of RunnableGraph[NotUsed]
    val stream = Source(fileList) // Emiting each element through the stream
        .map(new java.io.File(_)) // Maping filename to file
        .filter(_.exists()) // Filtering non-existing files
        .filter(_.length() != 0) // Filtering empty files
        .to(Sink.foreach(f => println(s"Absolute path: ${f.getAbsolutePath}")))
    
    stream.run()

}