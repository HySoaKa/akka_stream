package com.hysoaka

// GraphDSL -> Compose multiple stages into a single one
// GraphStage -> Cannot be decomposed into smaller pieces
// Shape defines the number of input = inlets and output = outlets ports in stage
// SourceShape -> 1 outlet
// SinkShape -> 1 inlet
// FlowShape -> 1 inlet + 1 outlet
// AmarphousShape -> * inlet + * outlet
// GraphStage include stage logic= define a set of handlers (InHandler OutHandler) that implement the behavior of different ports
//     -> The logic should extend GraphStageLogic and can be stateful or statless
//     -> Handlers are also part of the backpressure mechanism

import akka.stream.{Attributes, Outlet, SourceShape}
import akka.stream.stage._

// Custom source stage
class GraphStageStreamsSource extends GraphStage[SourceShape[String]] {
    val out: Outlet[String] = Outlet("SystemInputSource")
    override val shape = SourceShape(out)

    // Source emits elements in a stream
    // -> implement GraphStageLogic with an OutHandler
    override def createLogic(inheritedAttributes: Attributes) = new GraphStageLogic(shape) {
        setHandler(out, new OutHandler {
            // OutHandler provides the onPull method that will be called by the downstream stages
            override def onPull() = {
                val line = "Hello Akka Streams !"
                // We push a string
                push(out, line)
            }
        })
    }
}