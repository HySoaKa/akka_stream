package com.hysoaka

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, FlowShape}
import akka.stream.scaladsl.{Balance, Flow, GraphDSL, Merge, Sink, Source}
import scala.util.Random

/**
 * Demonstrating different execution boundries
 * - Default behavior that encapsulates the execution within a single actor,
 *   sequentil execution: the Sink pulls an element, this element gets executed by WashStage 
 *   then by DryStage and it reaches finally the Sink
 *   Only when the element is successfully processed by the Sink, another element is pulled
 *   from the Source
 * - Asynchronous boundries by providing async modifier to both WashStage and DryStage
 *   this allows elements to be executed asynchronously
 *   Elements no longer need to wait for both the stages to finish to be processed
 *   by the following stage
 * - Using GraphDSL to parallelize the process into three different branches
 */

trait PipeliningParallelizing extends App {
    implicit val actorSystem = ActorSystem("PipeliningParellelizing")
    implicit val actorMaterializer = ActorMaterializer()

    case class Wash(id: Int)
    case class Dry(id: Int)
    case class Done(id: Int)

    val tasks = (1 to 5).map(Wash)

    def washStage = Flow[Wash].map(wash => {
        val sleepTime = Random.nextInt(3) * 1000
        println(s"Washing ${wash.id}. It will take $sleepTime milliseconds.")
        Thread.sleep(sleepTime)
        Dry(wash.id)
    })

    def dryStage = Flow[Dry].map(dry => {
        val sleepTime = Random.nextInt(3) * 1000
        println(s"Drying ${dry.id}. It will take $sleepTime milliseconds.")
        Thread.sleep(sleepTime)
        Done(dry.id)
    })

    val parallelStage = Flow.fromGraph(
        GraphDSL.create() {implicit builder =>
            import GraphDSL.Implicits._

            // A Balance is a stage with one input port and n output ports that distributes
            // elements in the output ports as soon as there is demand
            val dispatchLaundry = builder.add(Balance[Wash](3)) // Three output ports
            // A Merge is a stage with n n input ports and one output port
            // it's responsible for merging different branches into unique channel
            val mergeLaundry = builder.add(Merge[Done](3)) // Three input ports

            dispatchLaundry.out(0) ~> washStage.async ~>
                dryStage.async ~> mergeLaundry.in(0)
            dispatchLaundry.out(1) ~> washStage.async ~>
                dryStage.async ~> mergeLaundry.in(1)
            dispatchLaundry.out(2) ~> washStage.async ~>
                dryStage.async ~> mergeLaundry.in(2)
            
            // Return FlowShape to indicate this stage behaves as a flow with one iput port
            // and one output port
            FlowShape(dispatchLaundry.in, mergeLaundry.out)
        }
    )

    def runGraph(testingFlow: Flow[Wash, Done, NotUsed]) = Source(tasks).via(testingFlow)
        .to(Sink.foreach(println)).run()
}