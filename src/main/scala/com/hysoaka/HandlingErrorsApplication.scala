package com.hysoaka

// Handling errors using supervision strategies
// Is's possible to set supervision strategies in two different levels: stream or stage

import akka.actor.ActorSystem
import akka.stream.{ActorAttributes, ActorMaterializer, ActorMaterializerSettings, Supervision}
import akka.stream.scaladsl._

object HandlingErrorsApplication extends App {
    implicit val actorSystem = ActorSystem("HandlingErrors")
    
    // Supervision startegy
    val streamDecider: Supervision.Decider = {
        case e: IndexOutOfBoundsException =>
            println("Dropping element because of IndexOutOfBoundException. Resuming.")
            Supervision.Resume // The elmt gets dropped and the stream keeps running and processing subsequent elmts
        case _ => Supervision.Stop // The stream is completed with a failure
    }

    // Supervision strategy
    val flowDecider: Supervision.Decider = {
        case e: IllegalArgumentException =>
            println("Dropping element because of IllegalArgumentException. Restarting.")
            Supervision.Restart // The elmt gets dropped and the stream keeps running however it restarts the stage beforehand. For statful stages the state will be lost since the restart happens by creating a new instance of the stage
        case _ => Supervision.Stop
    }

    // Setting the supervision strategy for all the stages in the stream by passing the decider instance as part of ActorMaterializerSettings
    val actorMaterializerSettings = ActorMaterializerSettings(actorSystem)
        .withSupervisionStrategy(streamDecider)
    implicit val actorMaterializer = ActorMaterializer(actorMaterializerSettings)
    val words = List("Handling", "Errors", "In", "Akka", "Streams", "")

    val flow = Flow[String].map(w => {
        if(w.length == 0) throw new IllegalArgumentException("Empty words are not allowed")
        w
    }).withAttributes(ActorAttributes.supervisionStrategy(flowDecider)) // Strategy only apply to specific stages

    Source(words).via(flow)
        .map(array => array(2))
        .to(Sink.foreach(println))
        .run()
}